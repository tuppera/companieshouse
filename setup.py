#!/usr/bin/env python

from distutils.core import setup

setup(
    name='companieshouse',
    version='1.0',
    description='Home-brewed Companies House SDK',
    author='Alex Tupper',
    author_email='alexjtupper@gmail.com',
    packages=['companieshouse'],
)