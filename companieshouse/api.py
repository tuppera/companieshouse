from traceback import format_exc
from typing import Optional

import requests
import time
# TODO: check if any more detailed requests.exceptions could give more useful information in handling
from requests.exceptions import RequestException

import base64

import companieshouse
from companieshouse import logger

# TODO: check that this makes sense both in thread, process and in cluster architecture
requests_session = requests.Session()


class APIRequest(object):
	def __init__(
			self,
			api_key: Optional[str] = None,
			api_url: Optional[str] = None
		):
		self.api_key = api_key or companieshouse.api_key
		self.api_url = api_url or companieshouse.api_url

	def __call__(self, method, relative_url, format: Optional[str] = 'json', **kwargs):
		"""
			If format is specified as JSON, return a dict, else -
			if format is not specified - return the response object
		:param method:
		:param relative_url:
		:param format:
		:param kwargs:
		:return:
		"""
		ts = time.time()
		url = self._construct_api_url(relative_url)
		logger.debug('Before API Call:\n\t- URL: %s\n\t- METHOD: %s' % (url, method))
		if 'headers' not in kwargs:
			kwargs['headers'] = {}
		kwargs['headers'].update(self._prepare_auth_header(self.api_key))

		try:
			result = requests_session.request(method, url, **kwargs)
			result.raise_for_status()
			if format == 'json':
				ret = result.json()
			else:
				ret = result
		except RequestException as e:
			logger.capture({'exception': e, 'context': 'APICall failed'})
			# TODO: this seems unnecessary?
			raise RequestException('APICall failed: %s' % format_exc())
		lap = time.time() - ts
		logger.capture({'context': 'APICall timestamp', 'data': {'lap_time': lap}})
		return ret

	def _construct_api_url(self, relative_url: str):
		assert relative_url.startswith('/'), f"relative_url must start with '/'; got: '%s'" % relative_url
		return '%s%s' % (self.api_url, relative_url)

	@classmethod
	def _prepare_auth_header(cls, api_key: str):
		auth = '%s:' % api_key
		auth = base64.b64encode(auth.encode()).decode()
		return {'Authorization': 'Basic %s' % auth}



