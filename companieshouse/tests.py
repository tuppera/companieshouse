from companieshouse.company import Company


APPTIVISM_LTD = '10543833'
company = Company(APPTIVISM_LTD)
print(company.profile)

incorporation = company.latest_filing('incorporation')
print(incorporation.meta)
print(incorporation.content)

confirmation_statement = company.latest_filing('confirmation-statement')
print(confirmation_statement.meta)
print(confirmation_statement.content)

accounts = company.latest_filing('accounts')
print(accounts.meta)
print(accounts.content)

