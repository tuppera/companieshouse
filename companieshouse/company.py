from typing import Union, Optional, List
from datetime import datetime

import requests
from requests import HTTPError

from companieshouse import logger, api_key
from companieshouse.api import APIRequest



# Resource doesn't exist - created here to bring obviously related resources together
class Company:
	def __init__(self, company_number: str, profile_items: Optional[dict] = None):
		"""
			profile_items: Some resources (e.g. CompanySearchResult)
			have CompanyProfile attributes already requested. We can reduce # requests by
			allowing a Company to be instantiated by company number AND cached results from
			these other resources.

		:param company_number:
		:param kwargs:
		"""
		self.company_number = company_number
		self.profile = CompanyProfile(self.company_number, profile_items)

		self._cached_documents = {}

	def __repr__(self):
		return '<CompaniesHouseCompany number:%s>' % self.company_number

	__str__ = __repr__

	@classmethod
	def search(cls, search_term: str) -> Optional[List['CompanySearchResult']]:
		request = APIRequest()
		try:
			data = request(
				'GET',
				'/search/companies',
				params=dict(q=search_term)
			)
			return [CompanySearchResult.from_item(item) for item in data['items']]
		except HTTPError as exc:
			if exc.request.status_code == 404:
				raise ValueError('Invalid company number')

	@property
	def incorporation_filing(self):
		return self.latest_filing('incorporation')

	@property
	def latest_confirmation_statement(self):
		return self.latest_filing('confirmation-statement')

	@property
	def latest_accounts(self):
		return self.latest_filing('accounts')

	def latest_filing(self, category:str):
		"""
		:param category: in
			'incorporation'
			'confirmation-statement'
			'accounts'
		:return:
		"""
		if category in self._cached_documents:
			return self._cached_documents[category]
		request = APIRequest()
		try:
			data = request(
				'GET',
				'/company/%s/filing-history?category=%s' % (self.company_number, category)
			)
			assert data['total_count'] >= 1, f'No file of that type'
			assert data.get('filing_history_status', '') == 'filing-history-available', f'Filing history not available'
			latest_filing = None
			for item in data['items']:
				if latest_filing is None:
					latest_filing = item
					continue
				if datetime.strptime(item['date'], '%Y-%m-%d') \
					> datetime.strptime(latest_filing['date'], '%Y-%m-%d'):
					latest_filing = item

			self._cached_documents[category] = Document(
				id=latest_filing['links']['document_metadata'].split('/')[-1],
				transaction_id=latest_filing['transaction_id'],
				company_number=self.company_number
			)
			return self._cached_documents[category]
		except HTTPError as exc:
			if exc.request.status_code == 404:
				raise ValueError('Invalid company number')
		except AssertionError as exc:
			# TODO: could do better here
			return None


# https://developer.companieshouse.gov.uk/api/docs/company/company_number/companyProfile-resource.html
class CompanyProfile:
	def __init__(self, company_number: str, profile_items: Optional[dict] = None):
		"""
			profile_items: Some resources (e.g. CompanySearchResult)
			have CompanyProfile attributes already requested. We can reduce # requests by
			allowing a Company to be instantiated by company number AND cached results from
			these other resources.

		:param company_number:
		:param kwargs:
		"""
		self.company_number = company_number
		self._cached_result = None
		self._profile_items = {} if profile_items is None else profile_items

	def __getitem__(self, item):
		if self._cached_result is None:
			try:
				self._cached_result = self.fetch(self.company_number)
				if self._cached_result is None:
					self._cached_result = {}
			except:
				self._cached_result = {}
		return self._cached_result.get(item)

	def fetch(self, company_number: str) -> 'CompanyProfile':
		request = APIRequest()
		try:
			return request(
				'GET',
				'/company/%s' % self.company_number
			)
		except HTTPError as exc:
			if exc.request.status_code == 404:
				raise ValueError('Invalid company number')


# https://developer.companieshouse.gov.uk/api/docs/search-overview/CompanySearch-resource.html
class CompanySearchResult:
	def __init__(self, item: dict = None):
		self.item = item

		company_number = item['company_number']
		company_name = item.get('company_name')
		if company_name is None:
			company_name = item.get('title')

		self.company_name = company_name
		self.company_number = company_number

		self.company = Company(
			company_number=company_number,
			profile_items=dict(company_name=company_name)
		)

	@classmethod
	def from_item(cls, item: dict):
		return cls(item=item)



# TODO: There is a missing entity: a filing - which we should then relate to document

#TODO: currently uses something outside of API to get content - could do better?
class Document:
	def __init__(self, id: str, company_number: str, transaction_id: str):
		self.id = id
		self.company_number = company_number
		self.transaction_id = transaction_id
		self.cached_meta = None
		self.cached_data = None

	@property
	def content(self):
		if self.cached_data is not None:
			return self.cached_data
		try:
			resp = requests.get(
				'https://beta.companieshouse.gov.uk/company/%s/filing-history/%s/document?format=pdf&download=0' % (
					self.company_number,
					self.transaction_id
				)
			)
			resp.raise_for_status()
			self.cached_data = resp.text
			return self.cached_data
		except HTTPError as exc:
			if exc.request.status_code == 404:
				raise ValueError('Invalid company number and/or transaction_id')

	@property
	def meta(self):
		if self.cached_meta is not None:
			return self.cached_meta
		try:
			req = APIRequest()
			resp = requests.get(
				'http://document-api.companieshouse.gov.uk/document/%s' % self.id,
				headers=APIRequest._prepare_auth_header(req.api_key)
			)
			resp.raise_for_status()
			self.cached_meta = resp.json()
			return self.cached_meta
		except HTTPError as exc:
			if exc.request.status_code == 404:
				raise ValueError('Invalid document_id')